package com.mitocode.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "signo")
public class Signo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSigno;
		
	@ManyToOne
	@JoinColumn(name = "id_paciente", nullable = false, foreignKey = @ForeignKey(name = "signo_paciente"))
	private Paciente paciente;
	
	// ISODate 2019-10-01T05:00:00.000
	@JsonSerialize(using = ToStringSerializer.class) 
	private LocalDateTime fecha;

	@ApiModelProperty(notes = "Temperatura debe tener minimo 2 caracteres")
	@Size(min = 2, max = 150, message = "Temperatura debe tener minimo 2 caracteres")
	@Column(name = "temperatura", nullable = false, length = 50)	
	private String temperatura;
	
	@ApiModelProperty(notes = "Pulso debe tener minimo 2 caracteres")
	@Size(min = 2, max = 50, message = "Pulso debe tener minimo 2 caracteres")
	@Column(name = "pulso", nullable = false, length = 50)	
	private String pulso;
	
	@ApiModelProperty(notes = "Ritmo respiratorio debe tener minimo 2 caracteres")
	@Size(min = 2, max = 50, message = "Ritmo respiratorio debe tener minimo 2 caracteres")
	@Column(name = "ritmo_respiratorio", nullable = false, length = 50)	
	private String ritmoRespiratorio;
	
	public int getIdSigno() {
		return idSigno;
	}

	public void setIdSigno(int idSigno) {
		this.idSigno = idSigno;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getPulso() {
		return pulso;
	}

	public void setPulso(String pulso) {
		this.pulso = pulso;
	}

	public String getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}

	public void setRitmoRespiratorio(String ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idSigno;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signo other = (Signo) obj;
		if (idSigno != other.idSigno)
			return false;
		return true;
	}
	
}
